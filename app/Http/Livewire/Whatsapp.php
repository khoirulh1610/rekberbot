<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Whatsapp extends Component
{
    public function render()
    {
        return view('livewire.whatsapp');
    }
}
