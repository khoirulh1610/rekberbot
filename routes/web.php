<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TripayController;
use App\Http\Controllers\HomeController;
use App\Models\Transaction;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/cek-wa', [HomeController::class,'cekwa']);
Route::post('/api/submit-form', [HomeController::class,'store']);


Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

// Route::get('/', [HomeController::class,'index']);
Route::get('/{urlkey?}', function ($urlkey = null)
{
    $data = Transaction::where('urlkey',$urlkey??'')->first(); 
        $secret_code = false;
        if ($data) {
            $secret_code = $data->secret_code;
        }       
    return view('welcome',compact('secret_code'));
});

